//
//  GameScene.swift
//  rectangle-movement
//
//  Created by Oktavia Citra on 29/09/20.
//  Copyright © 2020 Oktavia Citra. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    private var rectangle: SKSpriteNode?
    
    override func didMove(to view: SKView) {
        rectangle = SKSpriteNode(color: UIColor.systemPink, size: CGSize(width: 50, height: 50))
        rectangle?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        rectangle?.position = CGPoint(x: 0, y: 0)
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.addChild(rectangle ?? SKSpriteNode())
    }
    
    
    func touchDown(atPoint pos : CGPoint) {

    }
    
    func touchMoved(toPoint pos : CGPoint) {
        rectangle?.position.x = pos.x
        rectangle?.position.y = pos.y
    }
    
    func touchUp(atPoint pos : CGPoint) {

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
